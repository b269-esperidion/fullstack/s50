import {Card, Button } from 'react-bootstrap';

export default function CourseCard() {
  return (
    <Card style={{ width: '100%'  }} className="p-3">
   <Card.Title><h2 style={{ marginLeft: '18px' }}>Sample Course</h2></Card.Title>
        <Card.Body>
     		<Card.Text style={{marginBottom: 0  , fontWeight: 'bold'}} >
            	 Description:
       		 </Card.Text>
        	<Card.Text>
          		This is a sample course offering
       		 </Card.Text>
         	<Card.Text style={{margin: 0}}>
         		 Price:
        	</Card.Text>
        	 <Card.Text>
          		PHP 40,000
        	</Card.Text>
        	<Button variant="primary">Enroll</Button>
        </Card.Body>
        
   
    </Card>
  );
}

